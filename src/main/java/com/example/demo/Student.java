package com.example.demo;
//import java.beans.Transient;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.context.annotation.Bean;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

//import com.sun.istack.NotNull;

//import com.sun.istack.NotNull;

@Entity
@Table(name="RUser")
@Component
public class Student {
	
	//@NotEmpty(message="id cant be empty")
	
	//@NotEmpty(message="id cant be empty")
	//@Id
	//@Max()
	private int mobile;
	
//	@Pattern(regexp="[^0-9]*")
	@Size(min=3, max=30)
	@NotEmpty(message="username can't be empty")
//	@Column(name="name")
	private String user;
	
	@NotEmpty(message="email can't be empty")
	@Email(message="Enter valid email")
	@Id
	private String email;
	
	
	//@Min(18)
	//@Max(100)
	//@NotEmpty(message="name can't be empty")
	//private int age;
	
	//@Max(8)
	@NotEmpty
//	@Column(name="password")
//	@Pattern(regexp="/^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$/")
	private String pass;
	
//	public Student(String name, String password) {
		// TODO Auto-generated constructor stub
	//}

	@Transient
	private String conpass;
	
	public int getmobile() {
		return mobile;
	}
	
	public void setid(int mobile) {
		this.mobile=mobile;
	}
	
	public String getuser() {
		return user;
	}
	
	public void setuser(String user) {
		this.user=user;
	}
	public String getemail() {
		return email;
	}
	
	public void setemail(String email) {
		this.email=email;
	}
	//public int getAge() {
//	return age;
//	}
	
	//public void setAge(int age) {
//		this.age=age;
//	}
	//@Min(2)
//	@Max(8)

	
	public String getpass() {
	      return pass;
	   }
	   public void setpass(String pass) {
	      this.pass = pass;
	   }
	
	   
		public String getconpass() {
		      return conpass;
		   }
		   public void setconpass(String conpass) {
		      this.conpass = conpass;
		   }
	   
	   
	@Override
	public String toString() {
		return "Student [mobile=" +mobile+",user="+user+",email="+email+",conpass="+conpass+",pass="+pass+"]";
	}
	
	
	
	
	
		
}
